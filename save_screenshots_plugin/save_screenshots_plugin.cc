/* Code adapted from gazebo example plugin SaveScreenshotsPlugin
 * 
 * Adapted by Hamal Marino
 */

/*
 * Copyright (C) 2012-2015 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

// Include Rand.hh first due to compilation error on osx (boost #5010)
// https://svn.boost.org/trac/boost/ticket/5010
#include <gazebo/math/Rand.hh>
#include <gazebo/gui/GuiIface.hh>
#include <gazebo/rendering/rendering.hh>

#include <iostream>
#include <boost/filesystem.hpp>

#include "save_screenshots_plugin.hh"

#define RED "\033[0;31m"
#define PURPLE "\033[0;35m"
#define GREEN "\033[0;32m"
#define ORANGE "\033[0;33m"
#define YELLOW "\033[1;33m"
#define BLUE "\033[0;34m"
#define NC "\033[0m"
#define CLASS_NAMESPACE "SaveScreenshotsPlugin::"
#define DEBUG 0

namespace gazebo
{
    
    SaveScreenshotsPlugin::~SaveScreenshotsPlugin()
    {
        this->connections.clear();
        // if (this->userCam)
        //     this->userCam->EnableSaveFrame(false);
        this->userCam.reset();
    }

    void SaveScreenshotsPlugin::Load(int _argc, char** _argv)
    {
        this->connections.push_back(
            event::Events::ConnectPreRender(
                boost::bind(&SaveScreenshotsPlugin::Update, this)));
        
        if(getenv("HOME"))
            this->save_path_=getenv("HOME");
        boost::filesystem::path tmp("Pictures/gazebo_screenshots");
        if(this->save_path_.empty() )
        {
            boost::filesystem::path p(boost::filesystem::current_path());
            p /= tmp; //.append(tmp.begin(),tmp.end());
            std::cout << RED << CLASS_NAMESPACE << "Unable to retrieve $HOME folder! Screenshots will be saved in the folder " << p.c_str() << NC << std::endl;
            this->save_path_ = p.string();
        }
        else
        {
            boost::filesystem::path p(this->save_path_);
            p /= tmp; //.append(tmp.begin(),tmp.end());
            std::cout << GREEN << CLASS_NAMESPACE << "Screenshots will be saved in the folder " << p.c_str() << NC << std::endl;
            this->save_path_ = p.string();
        }
        
        save_count_ = 0;
        how_often_.Set(0.5);
        last_screen_.Set(-1.0);
        
        // Create a node for transportation
        this->node = transport::NodePtr(new transport::Node());
        this->node->Init("default");
        this->statsSub = this->node->Subscribe("~/world_stats",&SaveScreenshotsPlugin::OnStats, this);
        
#if DEBUG
        std::cout << GREEN << CLASS_NAMESPACE << __func__ << " : TopicNamespace = " << this->node->GetTopicNamespace() << NC << std::endl;
        std::cout << GREEN << CLASS_NAMESPACE << __func__ << " : Topic = " << this->statsSub->GetTopic() << NC << std::endl;
#endif
        
        // check whether ROS is active on the machine
        bool use_ros = false;
        if(getenv("ROS_MASTER_URI"))
        {
            ros::init(_argc,_argv,"gazebo_screenshots_plugin");
            if(ros::master::check())
                use_ros = true;
        }
        
        // if ROS is active, start ROS services
        if(use_ros)
        {
            ros_node_ = ros::NodeHandlePtr(new ros::NodeHandle("/gazebo_screenshots_plugin"));
            std::cout << GREEN << CLASS_NAMESPACE << __func__ << " : Started ROS services!" << NC << std::endl;
            ros_start_sub_ = ros_node_->subscribe<std_msgs::String>("start",1,&SaveScreenshotsPlugin::startRecording,this);
            ros_stop_sub_ = ros_node_->subscribe<std_msgs::Empty>("stop",1,&SaveScreenshotsPlugin::stopRecording,this);
            ros_delete_service_ = ros_node_->advertiseService("delete",&SaveScreenshotsPlugin::deleteScreenshots,this);
            
            saving_screenshots_ = false;
            aspin_ = new ros::AsyncSpinner(1);
            aspin_->start();
        }
        else
        {
            saving_screenshots_ = true;
            save_name_ = "gazebo_screenshots";
        }
    }

    void SaveScreenshotsPlugin::OnStats(ConstWorldStatisticsPtr& _msg)
    {
        const gazebo::msgs::Time& t = _msg->sim_time();
        sim_time_.Set(t.sec(),t.nsec());
        std::cout << ORANGE << CLASS_NAMESPACE << __func__ << " : Updated sim_time_! " << sim_time_ << NC << std::endl;
    }

    std::string SaveScreenshotsPlugin::GetFrameFilename()
    {
        std::string path = this->save_path_;
        boost::filesystem::path pathToFile;
        
        std::string friendlyName(save_name_);
        boost::replace_all(friendlyName, "::", "_");
        
        pathToFile = (path.empty()) ? "." : path;
        pathToFile /= boost::str(boost::format("%s-%04d.jpg")
        % friendlyName.c_str() % this->save_count_);
        this->save_count_++;
        
        // Create a directory if not present
        if (!boost::filesystem::exists(pathToFile.parent_path()))
            boost::filesystem::create_directories(pathToFile.parent_path());
        
        return pathToFile.string();
    }

    void SaveScreenshotsPlugin::Update()
    {
        if (!this->userCam)
        {
            // Get a pointer to the active user camera
            this->userCam = gui::get_active_camera();
            
            // // Enable saving frames
            // this->userCam->EnableSaveFrame(true);
            
            // Set the camera to be ad the desired location (found in Gazebo)
            gazebo::math::Pose p(1.0,0.5,0.6,0.0,0.3,-2.7);
            this->userCam->SetDefaultPose(p);
            
            // Specify the path to save frames into
            this->userCam->SetSaveFramePathname(this->save_path_);
            
            this->userCam->SetCaptureData(true);
        }
        
        // TODO: understand why in the *right* way doesn't work >> would be much better...
        static int counter = 0;
        counter = (counter+1)%25000;
        
        mutex_.lock();
        // if(sim_time_ - last_screen_ > how_often_)
        if(counter == 0 && saving_screenshots_)
        {
            // save a single frame
            std::string filename = this->GetFrameFilename();
            
            bool saved = this->userCam->SaveFrame(filename);
#if DEBUG
            if(saved)
                std::cout << BLUE << CLASS_NAMESPACE << __func__ << " : Saved screenshot! " << filename << " sim_time:" << sim_time_ << NC << std::endl;
            else
                std::cout << RED << CLASS_NAMESPACE << __func__ << " : Unable to save screenshot! " << sim_time_ << NC << std::endl;
#endif
            
            // update last_screen_ time
            while(sim_time_ - last_screen_ > how_often_)
                last_screen_ += how_often_;
        }
        mutex_.unlock();
        
        // // Test to find the correct pose for the camera
        // gazebo::math::Pose p = this->userCam->GetWorldPose();
        // std::cout << p << std::endl;
        
        // // Get scene pointer
        // rendering::ScenePtr scene = rendering::get_scene();
        // 
        // // Wait until the scene is initialized.
        // if (!scene || !scene->GetInitialized())
        //     return;
        // 
        // // Look for a specific visual by name.
        // if (scene->GetVisual("ground_plane"))
        //     std::cout << "Has ground plane visual\n";
    }
    
    void SaveScreenshotsPlugin::startRecording(const std_msgs::StringConstPtr& _msg)
    {
        std::unique_lock<std::mutex>(mutex_);
        
        if(!saving_screenshots_)
        {
            saving_screenshots_ = true;
            save_name_ = _msg->data;
            save_count_ = 0;
            ROS_DEBUG_STREAM_NAMED(CLASS_NAMESPACE,CLASS_NAMESPACE << __func__ << " : Starting to save screenshots named " << save_name_);
        }
        else
        {
            ROS_WARN_STREAM_NAMED(CLASS_NAMESPACE,CLASS_NAMESPACE << __func__ << " : Already saving screenshots, NOT starting with a new name! Stop saving before...");
        }
        return;
    }
    
    void SaveScreenshotsPlugin::stopRecording(const std_msgs::EmptyConstPtr& _msg)
    {
        std::unique_lock<std::mutex>(mutex_);
        
        if(saving_screenshots_)
        {
            saving_screenshots_ = false;
            ROS_DEBUG_STREAM_NAMED(CLASS_NAMESPACE,CLASS_NAMESPACE << __func__ << " : Stopped saving screenshots!");
        }
        return;
    }
    
    bool SaveScreenshotsPlugin::deleteScreenshots(std_srvs::TriggerRequest& _req, std_srvs::TriggerResponse& _res)
    {
        std::unique_lock<std::mutex>(mutex_);
        
        if(saving_screenshots_)
        {
            _res.success = false;
            _res.message = "Unable to delete screenshots while saving: stop saving them first!";
            return false;
        }
        
        uint count(save_count_);
        save_count_ = 0;
        uint undel_files = 0;
        while(save_count_ < count)
        {
            std::string name = GetFrameFilename();
            if(!boost::filesystem::remove(boost::filesystem::path(name)))
            {
                undel_files++;
            }
        }
        
        _res.success = (undel_files == 0);
        _res.message = "Deleted #" + std::to_string(count - undel_files) + " files";
        if(!_res.success)
            _res.message += " - remaining #" + std::to_string(undel_files);
        
        return true;
    }

    // Register this plugin with the simulator
    GZ_REGISTER_SYSTEM_PLUGIN(SaveScreenshotsPlugin)
}
