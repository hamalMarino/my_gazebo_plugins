/* Code adapted from gazebo example plugin SaveScreenshotsPlugin
 * 
 * Adapted by Hamal Marino
 */

/*
 * Copyright (C) 2012-2015 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

// Include Rand.hh first due to compilation error on osx (boost #5010)
// https://svn.boost.org/trac/boost/ticket/5010
#include <gazebo/gazebo.hh>
#include <gazebo/transport/transport.hh>

#include <string>
#include <mutex>

// ROS includes
#include "ros/ros.h"
#include <std_msgs/String.h>
#include <std_msgs/Empty.h>
#include <std_srvs/Trigger.h>

namespace gazebo
{
    class SaveScreenshotsPlugin : public SystemPlugin
    {
        /////////////////////////////////////////////
        /// \brief Destructor
        public: virtual ~SaveScreenshotsPlugin();
        
        /////////////////////////////////////////////
        /// \brief Called after the plugin has been constructed.
        public: void Load(int _argc, char ** _argv);
        
        /////////////////////////////////////////////
        /// \brief Called once after Load
        private: void Init() {};
        
        //////////////////////////////////////////////////
        private: std::string GetFrameFilename();
        
        /////////////////////////////////////////////////
        /// \brief Called every time the topic world_stats is received
        private: void OnStats(ConstWorldStatisticsPtr &_msg);
        
        /////////////////////////////////////////////
        /// \brief Called every PreRender event. See the Load function.
        private: void Update();
        
        /// Pointer the user camera.
        private: rendering::UserCameraPtr userCam;
        
        /// All the event connections.
        private: std::vector<event::ConnectionPtr> connections;
        
        /// \brief Node used to establish communication with gzserver.
        private: transport::NodePtr node;
        
        /// \brief Subscriber to world statistics messages.
        private: transport::SubscriberPtr statsSub;
        
        /// \brief ros function to start recording
        private: void startRecording(const std_msgs::StringConstPtr& _msg);
        
        /// \brief ros function to stop recording
        private: void stopRecording(const std_msgs::EmptyConstPtr& _msg);
        
        /// \brief ros function to delete last bunch of screenshots
        private: bool deleteScreenshots(std_srvs::TriggerRequest& _req, std_srvs::TriggerResponse& _res);
        
        private:
            std::string save_path_;
            std::string save_name_;
            // I need simulation time
            gazebo::common::Time last_screen_,sim_time_,how_often_;
            unsigned int save_count_;
            ros::NodeHandlePtr ros_node_;
            ros::AsyncSpinner* aspin_;
            ros::Subscriber ros_start_sub_,ros_stop_sub_;
            ros::ServiceServer ros_delete_service_;
            bool saving_screenshots_;
            std::mutex mutex_;
    };
}
