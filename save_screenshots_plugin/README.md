### SaveScreenshotsPlugin for Gazebo GUI

A plugin for automatically saving screenshots during the simulation, at a given rate.

Right now, the rate is given programmatically as the number of PreRender events to count before taking any screenshot; improving this would require using simulation time, for which the code is already present though still not working.

## Include the plugin

To use this plugin, you will start Gazebo GUI (gzclient) with the following command

```
$ gzclient -g libgazebo_save_screenshots_plugin.so
```
