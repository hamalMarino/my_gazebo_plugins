# my_gazebo_plugins

This repository contains a small set of gazebo plugins, aiming to be useful for visualization/debug purposes.

## Dependencies

Gazebo must be installed on the system: at present, this repository is tested on Gazebo 6.

## Include the plugin

To include each plugin, follow the specific instructions inside its directory.
