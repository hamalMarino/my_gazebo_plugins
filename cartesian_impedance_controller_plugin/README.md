### CartesianImpedanceControllerPlugin for Gazebo

A plugin for controlling the reference position and the stiffness/damping of a certain body. This is based on the [Hand of God plugin](https://github.com/ros-simulation/gazebo_ros_pkgs/blob/indigo-devel/gazebo_plugins/src/gazebo_ros_hand_of_god.cpp), and the code is adapted from there.

The main difference with that plugin is the fact that the spring is a fully customizable 6D spring, expressed in components local to the end-effector.

## Include the plugin

To use this plugin, you can add in your SDF file (or URDF <gazebo> tag) the following:

```
<plugin name="any_plugin_name" filename="libgazebo_cartesian_impedance_controller_plugin.so">
	<frameId>${my_frame_id}</frameId>
		<kl1>${x_linear_stiffness}</kl1>
		<kl2>${y_linear_stiffness}</kl2>
		<kl3>${z_linear_stiffness}</kl3>
		<ka1>${x_angular_stiffness}</ka1>
		<ka2>${y_angular_stiffness}</ka2>
		<ka3>${z_angular_stiffness}</ka3>
		<linkName>${the_link_to_control}</linkName>
</plugin>
```

Notice that the frame which you need to publish via TF, as with the Hand of God plugin, is `${my_frame_id}_desired`.
