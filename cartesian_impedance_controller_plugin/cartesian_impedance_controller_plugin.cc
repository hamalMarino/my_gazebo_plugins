/* Code adapted from gazebo Hand of God plugin 
 * https://github.com/ros-simulation/gazebo_ros_pkgs/blob/indigo-devel/gazebo_plugins/src/gazebo_ros_hand_of_god.cpp
 * 
 * Adapted by Hamal Marino
 */

/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2013, Open Source Robotics Foundation
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Open Source Robotics Foundation
 *     nor the names of its contributors may be
 *     used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/**
 *  \author Jonathan Bohren
 *  \desc   A "hand-of-god" plugin which drives a floating object around based
 *  on the location of a TF frame. This plugin is useful for connecting human input
 *  devices to "god-like" objects in a Gazebo simulation.
 */

#include "cartesian_impedance_controller_plugin.hh"
#include <ros/ros.h>

// #include <sdf/Element.hh>

namespace gazebo
{
    // Register this plugin with the simulator
    GZ_REGISTER_MODEL_PLUGIN(GazeboRosCartesianImpedanceController);
    
    ////////////////////////////////////////////////////////////////////////////////
    // Constructor
    GazeboRosCartesianImpedanceController::GazeboRosCartesianImpedanceController() :
    ModelPlugin(),
    robot_namespace_(""),
    frame_id_("hog"),
    kl1_(200), kl2_(200), kl3_(200),
    ka1_(200), ka2_(200), ka3_(200)
    {
    }
    
    ////////////////////////////////////////////////////////////////////////////////
    // Destructor
    GazeboRosCartesianImpedanceController::~GazeboRosCartesianImpedanceController()
    {
    }
    
    ////////////////////////////////////////////////////////////////////////////////
    // Load the controller
    void GazeboRosCartesianImpedanceController::Load( physics::ModelPtr _parent, sdf::ElementPtr _sdf )
    {
        // Make sure the ROS node for Gazebo has already been initalized
        if (!ros::isInitialized()) {
            ROS_FATAL_STREAM("A ROS node for Gazebo has not been initialized, unable to load plugin. "
            << "Load the Gazebo system plugin 'libgazebo_ros_api_plugin.so' in the gazebo_ros package)");
            return;
        }
        
        // Get sdf parameters
        if(_sdf->HasElement("robotNamespace")) {
            this->robot_namespace_ = _sdf->Get<std::string>("robotNamespace") + "/";
        }  
        
        if(_sdf->HasElement("frameId")) {
            this->frame_id_ = _sdf->Get<std::string>("frameId");
        }
        
        if(_sdf->HasElement("kl1") && _sdf->HasElement("kl2") && _sdf->HasElement("kl3")) {
            this->kl1_ = _sdf->Get<double>("kl1");
            this->kl2_ = _sdf->Get<double>("kl2");
            this->kl3_ = _sdf->Get<double>("kl3");
        }
        if(_sdf->HasElement("ka1") && _sdf->HasElement("ka2") && _sdf->HasElement("ka3")) {
            this->ka1_ = _sdf->Get<double>("ka1");
            this->ka2_ = _sdf->Get<double>("ka2");
            this->ka3_ = _sdf->Get<double>("ka3");
        }
        
        if(_sdf->HasElement("linkName")) {
            this->link_name_ = _sdf->Get<std::string>("linkName");
        } else {
            ROS_FATAL_STREAM("The hand-of-god plugin requires a `linkName` parameter tag");
            return;
        }
        
        // Store the model
        model_ = _parent;
        
        // Disable gravity for the hog
        model_->SetGravityMode(false);
        
        // Get the floating link
        floating_link_ = model_->GetLink(link_name_);
        if(!floating_link_) {
            ROS_ERROR("Floating link not found!");
            const std::vector<physics::LinkPtr> &links = model_->GetLinks();
            for(unsigned i=0; i < links.size(); i++) {
                ROS_ERROR_STREAM(" -- Link "<<i<<": "<<links[i]->GetName());
            }
            return;
        }
        
        // apply damping based on the stiffness
        cl1_ = 2.0 * sqrt(kl1_*floating_link_->GetInertial()->GetMass());
        cl2_ = 2.0 * sqrt(kl2_*floating_link_->GetInertial()->GetMass());
        cl3_ = 2.0 * sqrt(kl3_*floating_link_->GetInertial()->GetMass());
        ca1_ = 2.0 * sqrt(ka1_*floating_link_->GetInertial()->GetIXX());
        ca2_ = 2.0 * sqrt(ka2_*floating_link_->GetInertial()->GetIYY());
        ca3_ = 2.0 * sqrt(ka3_*floating_link_->GetInertial()->GetIZZ());
        
        kl_.Set(kl1_,kl2_,kl3_);
        ka_.Set(ka1_,ka2_,ka3_);
        cl_.Set(cl1_,cl2_,cl3_);
        ca_.Set(ca1_,ca2_,ca3_);
        
        // Create the TF listener for the desired position of the hog
        tf_buffer_.reset(new tf2_ros::Buffer());
        tf_listener_.reset(new tf2_ros::TransformListener(*tf_buffer_));
        tf_broadcaster_.reset(new tf2_ros::TransformBroadcaster());
        
        // Register update event handler
        this->update_connection_ = event::Events::ConnectWorldUpdateBegin(
            boost::bind(&GazeboRosCartesianImpedanceController::GazeboUpdate, this));
    }
    
    ////////////////////////////////////////////////////////////////////////////////
    // Update the controller
    void GazeboRosCartesianImpedanceController::GazeboUpdate()
    {
        // Get TF transform relative to the /world link
        geometry_msgs::TransformStamped hog_desired_tform;
        static bool errored = false;
        try{
            hog_desired_tform = tf_buffer_->lookupTransform("world", frame_id_+"_desired", ros::Time(0));
            errored = false;
        } catch (tf2::TransformException ex){
            if(!errored && last_error_.compare(ex.what()) != 0) {
                last_error_ = ex.what();
                ROS_ERROR("%s",ex.what());
                errored = true;
            } 
            return;
        }
        
        // Convert TF transform to Gazebo Pose
        const geometry_msgs::Vector3 &p = hog_desired_tform.transform.translation;
        const geometry_msgs::Quaternion &q = hog_desired_tform.transform.rotation;
        gazebo::math::Pose hog_desired(
            gazebo::math::Vector3(p.x, p.y, p.z),
                                       gazebo::math::Quaternion(q.w, q.x, q.y, q.z));
        
#define TEST_FORCE 1
#define TEST_TORQUE 0
        
        // Relative transform from actual to desired pose
        gazebo::math::Pose world_pose = floating_link_->GetDirtyPose();
#if (TEST_FORCE == 0)
        gazebo::math::Vector3 err_pos = hog_desired.pos - world_pose.pos;
        gazebo::math::Vector3 world_force = kl_ * err_pos - cl_ * floating_link_->GetWorldLinearVel();
#else
        gazebo::math::Pose desired_world = hog_desired.GetInverse();
        // pose between desired and actual: error frame, expressed in <desired> frame
        gazebo::math::Pose desired_actual = desired_world * world_pose;
        gazebo::math::Vector3 err_pos = desired_world.rot.GetAsMatrix3() * (hog_desired.pos - world_pose.pos);
        // gazebo::math::Vector3 err_pos = desired_actual.pos * -1.0;
        gazebo::math::Vector3 err_desired_lin_vel = desired_world.rot.GetAsMatrix3() * floating_link_->GetWorldLinearVel(); // floating_link_->GetWorldLinearVel());
        // ATTENTION: missing part of the adjoint transform
        // remember to transform everything in the right frame
        gazebo::math::Vector3 world_force = desired_world.rot.GetInverse().GetAsMatrix3() * ( kl_ * err_pos - cl_ * err_desired_lin_vel);
#endif
#if (TEST_TORQUE == 0)
        // Get exponential coordinates for rotation
        gazebo::math::Quaternion err_rot = (world_pose.rot.GetAsMatrix4().Inverse() * hog_desired.rot.GetAsMatrix4()).GetRotation();
        gazebo::math::Quaternion not_a_quaternion = err_rot.GetLog();

        gazebo::math::Vector3 actual_torque = ka_ * gazebo::math::Vector3(not_a_quaternion.x, not_a_quaternion.y, not_a_quaternion.z) - ca_ * floating_link_->GetRelativeAngularVel();
#else
        // Get exponential coordinates for rotation
        gazebo::math::Quaternion err_rot = (world_pose.rot.GetAsMatrix4().Inverse() * hog_desired.rot.GetAsMatrix4()).GetRotation();
        // NOTE: the following should be equal, but it's not...
        // gazebo::math::Quaternion err_rot_new = desired_actual.rot.GetInverse();
        // std::cout << "quat_diff: [" << err_rot_new.x - err_rot.x << " " << err_rot_new.y - err_rot.y << " " << err_rot_new.z - err_rot.z << " " << err_rot_new.w - err_rot.w << std::endl;
        gazebo::math::Quaternion not_a_quaternion = err_rot.GetLog();
        gazebo::math::Vector3 err_rot3 = gazebo::math::Vector3(not_a_quaternion.x, not_a_quaternion.y, not_a_quaternion.z);
        err_rot3 = desired_actual.rot.GetAsMatrix3() * err_rot3;
        // also add a component for the velocity
        gazebo::math::Vector3 relative_rot_vel = desired_actual.rot.GetAsMatrix3() * floating_link_->GetRelativeAngularVel();
        
        // remember to transform everything in the right frame
        gazebo::math::Vector3 actual_torque = desired_actual.rot.GetInverse().GetAsMatrix3() * (ka_ * err_rot3 - ca_ * relative_rot_vel);
#endif
        floating_link_->AddForce(world_force);
        floating_link_->AddRelativeTorque(actual_torque);
        
        // Convert actual pose to TransformStamped message
        geometry_msgs::TransformStamped hog_actual_tform;
        
        hog_actual_tform.header.frame_id = "world";
        hog_actual_tform.header.stamp = ros::Time::now();
        
        hog_actual_tform.child_frame_id = frame_id_ + "_actual";
        
        hog_actual_tform.transform.translation.x = world_pose.pos.x;
        hog_actual_tform.transform.translation.y = world_pose.pos.y;
        hog_actual_tform.transform.translation.z = world_pose.pos.z;
        
        hog_actual_tform.transform.rotation.w = world_pose.rot.w;
        hog_actual_tform.transform.rotation.x = world_pose.rot.x;
        hog_actual_tform.transform.rotation.y = world_pose.rot.y;
        hog_actual_tform.transform.rotation.z = world_pose.rot.z;
        
        tf_broadcaster_->sendTransform(hog_actual_tform);
    }
    
}
